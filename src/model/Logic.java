package model;

import java.util.ArrayList;
import java.util.Optional;


public class Logic {
    private State state;
    private Board board;

    Logic() {
        state = new State();
        board = new Board();
    }

    public ArrayList<Move> legalMoves() {
        return legalMoves(state.getTurn());
    }

    public void makeMove(Move move) {
        if (!isMoveLegal(move)) throw new RuntimeException("Illegal move");
        board.makeMove(move);
        state.addMove(move);
        // TODO: Check end of turn condition and change turn
    }

    public void undoMove() {
        Move lastMove = state.getLastMove();
        state.removeLastMove();
        board.undoMove(lastMove);
    }

    public int computeRating() {
        return computeRating(state.getTurn());
    }

    public int computeRating(Color color) {
        return board.getPlayerPawns(color).size();
    }

    private Boolean isMoveLegal(Move move) {
        return legalMoves().contains(move);
    }

    private ArrayList<Move> legalMoves(Color player) {
        ArrayList<Move> moves;

        if (state.isPawnSelected()) {
            Move lastMove = state.getLastMove();
            Pawn pawn = lastMove.getPawn();
            moves = capturingMoves(pawn);

            // Remove moves in the same direction like the last one
            moves.removeIf(move -> move.direction().equals(lastMove.direction()));
            // Remove moves to the same node
            moves.removeIf(move -> {
                for (Move previousMove : state.getTurnMoves()) {
                    if (move.getTargetPosition().equals(previousMove.getInitialPosition())) return true;
                    if (move.getTargetPosition().equals(previousMove.getTargetPosition())) return true;
                }
                return false;
            });
            // TODO: add end of turn move
        } else {
            moves = capturingMoves(player);
            if (moves.isEmpty()) moves = nonCapturingMoves(player);
        }

        return moves;
    }

    private ArrayList<Move> capturingMoves(Color player) {
        ArrayList<Move> capturingMoves = new ArrayList<>();
        for (Pawn pawn : board.getPlayerPawns(player)) {
            capturingMoves.addAll(capturingMoves(pawn));
        }
        return capturingMoves;
    }

    private ArrayList<Move> nonCapturingMoves(Color player) {
        ArrayList<Move> nonCapturingMoves = new ArrayList<>();
        for (Pawn pawn : board.getPlayerPawns(player)) {
            nonCapturingMoves.addAll(nonCapturingMoves(pawn));
        }
        return nonCapturingMoves;
    }

    private ArrayList<Move> capturingMoves(Pawn pawn) {
        ArrayList<Move> nonCapturingMoves = potentialMoves(pawn);
        nonCapturingMoves.removeIf(move -> !move.isCapturing());
        return nonCapturingMoves;
    }

    private ArrayList<Move> nonCapturingMoves(Pawn pawn) {
        ArrayList<Move> nonCapturingMoves = potentialMoves(pawn);
        nonCapturingMoves.removeIf(move -> move.isCapturing());
        return nonCapturingMoves;
    }

    private ArrayList<Move> potentialMoves(Pawn pawn) {
        ArrayList<Move> moves = new ArrayList<>();
        Node node = board.getNode(pawn);

        for (Node neighbour : board.emptyNeighbours(pawn)) {
            Move move = new Move(node.getPosition(), neighbour.getPosition(), pawn);

            // Capture by approach
            ArrayList<Node> nodesInDirection = board.nodesInDirection(node, move.direction());
            nodesInDirection.remove(0); // Remove target node
            ArrayList<Pawn> capturedPawns = capturePawns(pawn, nodesInDirection);

            // Capture by withdraw
            nodesInDirection = board.nodesInDirection(node, move.direction().opposite());
            capturedPawns.addAll(capturePawns(pawn, nodesInDirection));

            move.addCaptured(capturedPawns, board.pawnsPositions(capturedPawns));
            moves.add(move);
        }

        return moves;
    }

    private ArrayList<Pawn> capturePawns(Pawn initialPawn, ArrayList<Node> nodes) {
        ArrayList<Pawn> captured = new ArrayList<>();
        for (Node node : nodes) {
            Optional<Pawn> pawn = board.getPawn(node);

            if (!pawn.isPresent()) break;
            else if (pawn.get().getColor().equals(initialPawn.getColor())) break;
            else captured.add(pawn.get());
        }

        return captured;
    }

    private void changeTurn() {
        state.changeTurn();
    }


}
