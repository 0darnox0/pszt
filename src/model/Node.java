package model;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;


public class Node {

    private final Position position;
    private Map<Direction, Node> connections; // keeps possible nodes to move to

    public Node(Position position) {
        this.position = position;
        this.connections = new EnumMap<>(Direction.class);
    }

    public Position getPosition() {
        return position;
    }

    public Collection<Node> getConnections() {
        return connections.values();
    }

    public Optional<Node> getConnection(Direction direction) {
        return Optional.ofNullable(connections.get(direction));
    }

    // adds to the list node, which is connected with considered node
    public void addConnection(Node node, Direction direction) {
        connections.put(direction, node);
    }

    public boolean isConnection(Node secondNode) {
        return connections.containsValue(secondNode);
    }

    public enum Direction {
        TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT, TOP_LEFT;

        public static Direction direction(Position initial, Position target) {
            int x = target.getX() - initial.getX();
            int y = target.getY() - initial.getY();

            if (x == 0 && y > 0) return TOP;
            else if (x > 0 && y > 0) return TOP_RIGHT;
            else if (x > 0 && y == 0) return RIGHT;
            else if (x > 0 && y < 0) return BOTTOM_RIGHT;
            else if (x == 0 && y < 0) return BOTTOM;
            else if (x < 0 && y < 0) return BOTTOM_LEFT;
            else if (x < 0 && y == 0) return LEFT;
            else if (x < 0 && y > 0) return TOP_LEFT;
            else throw new NotImplementedException();
        }

        public Direction opposite() {
            if (this == TOP) return BOTTOM;
            else if (this == TOP_RIGHT) return BOTTOM_LEFT;
            else if (this == RIGHT) return LEFT;
            else if (this == BOTTOM_RIGHT) return TOP_LEFT;
            else if (this == BOTTOM) return TOP;
            else if (this == BOTTOM_LEFT) return TOP_RIGHT;
            else if (this == LEFT) return RIGHT;
            else if (this == TOP_LEFT) return BOTTOM_RIGHT;
            else throw new NotImplementedException();
        }
    }
}
