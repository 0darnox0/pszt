package model;

import java.util.ArrayList;

public class State {
    private Color turn;
    private ArrayList<Move> turnMoves;

    State() {
        turn = Color.WHITE;
        turnMoves = new ArrayList<>();
    }

    public Color getTurn() {
        return turn;
    }

    public void changeTurn() {
        turn = turn.opposite();

        turnMoves.clear();
    }

    public void addMove(Move move) {
        turnMoves.add(move);
    }

    public Move getLastMove() {
        return turnMoves.get(turnMoves.size() - 1);
    }

    public void removeLastMove() {
        turnMoves.remove(turnMoves.size() - 1);
    }

    public ArrayList<Move> getTurnMoves() {
        return turnMoves;
    }

    public boolean isPawnSelected() {
        return !turnMoves.isEmpty();
    }
}
