package model;

import model.Node.Direction;

import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;

import static model.Color.BLACK;
import static model.Color.WHITE;

// Class represents board of size 5x9.
public class Board {
    public final static int BOARD_HEIGHT = 5;
    public final static int BOARD_WIDTH = 9;
    /*  BOARD:
        00 01 02 03 04 05 06 07 08
        09 10 11 12 13 14 15 16 17
        18 19 20 21 22 23 24 25 26
        27 28 29 30 31 32 33 34 35
        36 37 38 39 40 41 42 43 44

        START: PAWNS COLORS:
        CC CC CC CC CC CC CC CC CC
        CC CC CC CC CC CC CC CC CC
        CC BB CC BB    CC BB CC BB
        BB BB BB BB BB BB BB BB BB
        BB BB BB BB BB BB BB BB BB */


    private Node[][] nodes; //table with Nodes
    private HashMap<Node, Pawn> node2Pawn;
    private HashMap<Pawn, Node> pawn2Node;

    Board() {
        nodes = new Node[BOARD_WIDTH][BOARD_HEIGHT];
        node2Pawn = new HashMap<>();
        pawn2Node = new HashMap<>();
        initializeBoard();
//        printConnections();
        printColorsOnBoard();
    }

    public void makeMove(Move move) {
        movePawn(move.getPawn(), move.getTargetPosition());

        // Capture pawns
        for (Pawn captured : move.getCaptured()) {
            removePawn(captured);
        }
    }

    public void undoMove(Move move) {
        movePawn(move.getPawn(), move.getInitialPosition());

        // Restore captured pawns
        Iterator capturedIterator = move.getCaptured().iterator();
        Iterator positionsIterator = move.getCapturedPositions().iterator();
        while (capturedIterator.hasNext() && positionsIterator.hasNext()) {
            Pawn pawn = (Pawn) capturedIterator.next();
            Position position = (Position) positionsIterator.next();
            addPawn(pawn, position);
        }
    }

    private void movePawn(Pawn pawn, Position targetPosition) {
        removePawn(pawn);
        addPawn(pawn, targetPosition);
    }

    private void addPawn(Pawn pawn, Position position) {
        Node targetNode = getNode(position);

        assert node2Pawn.get(targetNode) == null;  // Check if position is empty
        assert pawn2Node.get(pawn) == null;  // Check if the pawn isn't already defined
        node2Pawn.put(targetNode, pawn);
        pawn2Node.put(pawn, targetNode);
    }

    private void removePawn(Pawn pawn) {
        Node initialNode = getNode(pawn);

        assert node2Pawn.get(initialNode).equals(pawn);  // Check if the pawn is on initialPosition
        node2Pawn.remove(initialNode);
        pawn2Node.remove(pawn);
    }

    public ArrayList<Pawn> getPlayerPawns(Color player) {
        ArrayList<Pawn> pawns = getPawns();
        pawns.removeIf(pawn -> !pawn.getColor().equals(player));
        return pawns;
    }

    public ArrayList<Pawn> getPawns() {
        return (ArrayList<Pawn>) node2Pawn.values();
    }

    public ArrayList<Node> nodesInDirection(Node initialNode, Direction direction) {
        ArrayList<Node> nodes = new ArrayList<>();

        Optional<Node> nextNode = initialNode.getConnection(direction);
        while (nextNode.isPresent()) {
            nodes.add(nextNode.get());
            nextNode = nextNode.get().getConnection(direction);
        }

        return nodes;
    }

    public ArrayList<Node> emptyNeighbours(Pawn pawn) {
        ArrayList<Node> emptyNeighbours = new ArrayList<>();

        Node node = getNode(pawn);
        for (Node neighbour : node.getConnections()) {
            Optional<Pawn> neighbourPawn = getPawn(neighbour);
            if (!neighbourPawn.isPresent()) emptyNeighbours.add(neighbour);
        }

        return emptyNeighbours;
    }

    public ArrayList<Node> occupiedNeighbours(Pawn pawn) {
        ArrayList<Node> occupiedNeighbours = new ArrayList<>();

        Node node = getNode(pawn);
        for (Node neighbour : node.getConnections()) {
            Optional<Pawn> neighbourPawn = getPawn(neighbour);
            if (neighbourPawn.isPresent()) occupiedNeighbours.add(neighbour);
        }

        return occupiedNeighbours;
    }

    public ArrayList<Position> pawnsPositions(ArrayList<Pawn> pawns) {
        ArrayList<Position> positions = new ArrayList<>();
        for (Pawn pawn : pawns) {
            positions.add(pawnPosition(pawn));
        }
        return positions;
    }

    public Position pawnPosition(Pawn pawn) {
        Node node = getNode(pawn);
        return node.getPosition();
    }

    //returns pointed node
    public Node getNode(Position position) {
        return nodes[position.getX()][position.getY()];
    }

    public Node getNode(Pawn pawn) {
        return pawn2Node.get(pawn);
    }

    //returns pawn on position
    public Optional<Pawn> getPawn(Position position) {
        return Optional.ofNullable(node2Pawn.get(getNode(position)));
    }

    public Optional<Pawn> getPawn(Node node) {
        return Optional.ofNullable(node2Pawn.get(node));
    }

    private void initializeBoard() {
        setNodes();
        setConnections();
        setPawns();
    }

    private void setConnections() {
        for (int x = 0; x < BOARD_WIDTH; x++) {
            for (int y = 0; y < BOARD_HEIGHT; y++) {
                // adds connection to the node on the right
                if (x < 8) nodes[x][y].addConnection(nodes[x + 1][y], Direction.RIGHT);
                // adds connection to the node on the left
                if (x >= 1) nodes[x][y].addConnection(nodes[x - 1][y], Direction.LEFT);
                // adds connection to the node below
                if (y >= 1) nodes[x][y].addConnection(nodes[x][y - 1], Direction.BOTTOM);
                // adds connection to the node on the above
                if (y < 4) nodes[x][y].addConnection(nodes[x][y + 1], Direction.TOP);
            }
        }
        for (int x = 0; x < BOARD_WIDTH; x++) {
            for (int y = 0; y < BOARD_HEIGHT; y++) {
                if ((y * BOARD_WIDTH + x) % 2 != 0) continue;
                // adds connection to the node below on the left
                if (y > 0 && x > 0) nodes[x][y].addConnection(nodes[x - 1][y - 1], Direction.BOTTOM_LEFT);
                // adds connection to the node below on the right
                if (y > 0 && x < 8) nodes[x][y].addConnection(nodes[x + 1][y - 1], Direction.BOTTOM_RIGHT);
                // adds connection to the node above on the right
                if (y < 4 && x < 8) nodes[x][y].addConnection(nodes[x + 1][y + 1], Direction.TOP_RIGHT);
                // adds connection to the node above on the left
                if (y < 4 && x > 0) nodes[x][y].addConnection(nodes[x - 1][y + 1], Direction.TOP_LEFT);
            }
        }
    }

    private void setPawns() {
        // Set white on top
        for (int y = 0; y < 2; y++) {
            for (int x = 0; x < BOARD_WIDTH; x++) {
                Pawn pawn = new Pawn(WHITE);
                addPawn(pawn, new Position(x, y));
            }
        }

        // Set first half of middle row
        for (int x = 0; x < BOARD_WIDTH / 2; x++) {
            Pawn pawn = x % 2 == 0 ? new Pawn(WHITE) : new Pawn(BLACK);

            addPawn(pawn, new Position(x, 2));
        }

        // Set second half of middle row
        for (int x = BOARD_WIDTH / 2 + 1; x < BOARD_WIDTH; x++) {
            Pawn pawn = x % 2 == 1 ? new Pawn(WHITE) : new Pawn(BLACK);

            addPawn(pawn, new Position(x, 2));
        }

        // Set black on bottom
        for (int y = 3; y < BOARD_HEIGHT; y++) {
            for (int x = 0; x < BOARD_WIDTH; x++) {
                Pawn pawn = new Pawn(BLACK);
                addPawn(pawn, new Position(x, y));
            }
        }
    }

    private void setNodes() {
        for (int x = 0; x < BOARD_WIDTH; x++) {
            for (int y = 0; y < BOARD_HEIGHT; y++) {
                nodes[x][y] = new Node(new Position(x, y));
                System.out.println(x + " " + y);
            }
        }
    }

    // for testing purposes: prints pawns' colors on board
    public void printColorsOnBoard() {
        System.out.println("*******************");
        for (int x = 0; x < BOARD_WIDTH; x++) {
            for (int y = 0; y < BOARD_HEIGHT; y++) {
                if (getPawn(new Position(x, y)).isPresent()) {
                    Color color = getPawn(new Position(x, y)).get().getColor();
                    if (color.equals(WHITE)) {
                        System.out.print("W ");
                    } else if (color.equals(BLACK)) {
                        System.out.print("B ");
                    } else {
                        System.out.print("  ");
                    }
                }
                System.out.println();
            }
            System.out.println("*******************");
        }
    }

//    public void printConnections() {
//        for (Node n : nodes) {
//            for (Integer i : n.getConnections()) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
//        }
//    }
}


