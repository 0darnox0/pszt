package model;


import model.Node.Direction;

import java.util.ArrayList;

public class Move {
    private Position initialPosition;
    private Position targetPosition;
    private Pawn pawn;
    private ArrayList<Pawn> captured;
    private ArrayList<Position> capturedPositions;

    public Move(Position initialPosition, Position targetPosition, Pawn pawn) {
        this.initialPosition = initialPosition;
        this.targetPosition = targetPosition;
        this.pawn = pawn;
        captured = new ArrayList<>();
        capturedPositions = new ArrayList<>();
    }

    public Pawn getPawn() {
        return pawn;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }

    public Position getTargetPosition() {
        return targetPosition;
    }

    public void addCaptured(Pawn pawn, Position position) {
        captured.add(pawn);
        capturedPositions.add(position);
    }

    public void addCaptured(ArrayList<Pawn> pawns, ArrayList<Position> positions) {
        assert pawns.size() == positions.size();
        captured.addAll(pawns);
        capturedPositions.addAll(positions);
    }

    public ArrayList<Pawn> getCaptured() {
        return captured;
    }

    public ArrayList<Position> getCapturedPositions() {
        return capturedPositions;
    }

    public Direction direction() {
        return Direction.direction(targetPosition, initialPosition);
    }

    public Boolean isCapturing() {
        return !captured.isEmpty();
    }

}
