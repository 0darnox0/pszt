import model.Board;
import model.Logic;
import model.Node;
import model.State;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("START THE GAME!");
        Board b = new Board();

        while(true){
            Board.printColorsOnBoard();
            System.out.println("Give position or -1 to change turn: ");
            Scanner sc = new Scanner(System.in);
            int position = sc.nextInt();
            Logic.processTurn(position);
        }

    }
}
//        State state = new State();
//        state.setSelected(new Node(4,2));
//        if(Logic.isCapturePossible())
//            System.out.println("Huraaaaa");
//    }
//}
