package algorithm;

import model.Logic;
import model.Move;

public class MinMax {
    private static final int MAX_DEPTH = 10;
    private Logic game;
    private Move bestMove;

    public MinMax(Logic game) {
        this.game = game;
    }

    /*** Choose the best move for the given player ***/
    public Move chooseMove() {
        maximizer(Integer.MIN_VALUE, Integer.MAX_VALUE, MAX_DEPTH);
        return bestMove;
    }

    private int maximizer(int alpha, int beta, int depth) {
        if (depth == 0) return game.computeRating();

        for (Move move : game.legalMoves()) {
            game.makeMove(move);
            int min = minimizer(alpha, beta, depth - 1);
            if (alpha > min) {
                alpha = min;
                if (depth == MAX_DEPTH) {
                    bestMove = move;
                }
            }
            game.undoMove();

            if (alpha >= beta) return beta;
        }

        return alpha;
    }

    private int minimizer(int alpha, int beta, int depth) {
        if (depth == 0) return game.computeRating();

        for (Move move : game.legalMoves()) {
            game.makeMove(move);
            beta = Math.min(beta, maximizer(alpha, beta, depth - 1));
            game.undoMove();

            if (alpha >= beta) return alpha;
        }

        return beta;
    }

}
