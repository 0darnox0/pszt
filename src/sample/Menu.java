package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Menu extends Application {

    @FXML
    private VBox vBox = new VBox();
    @FXML
    public Button roundButton = new Button();

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        primaryStage.setTitle("AKM Studio - Fanorona");
        primaryStage.setScene(new Scene(root, 600, 400));
        Scene scene = primaryStage.getScene();
        scene.getStylesheets().add("sample/cssButton.css");
        primaryStage.show();
    }
}
