package sample;

/**
 * Zeby FxBoardCcontroller bez problemu wspolpracowal z plikami BoardGui.fxml, BoardGui.java musimy importowac pliki z pakiety javafx a nie java.awt!!! (np. dla Label, Button itd.)
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.Board;
import model.Color;
import model.Logic;
import model.State;

public class FxBoardController {

    @FXML
    private GridPane grid = new GridPane();
    @FXML
    private Button b0 = new Button();
    @FXML
    private Button b1 = new Button();
    @FXML
    private Button b2 = new Button();
    @FXML
    private Button b3 = new Button();
    @FXML
    private Button b4 = new Button();
    @FXML
    private Button b5 = new Button();
    @FXML
    private Button b6 = new Button();
    @FXML
    private Button b7 = new Button();
    @FXML
    private Button b8 = new Button();
    @FXML
    private Button b9 = new Button();
    @FXML
    private Button b10 = new Button();
    @FXML
    private Button b11 = new Button();
    @FXML
    private Button b12 = new Button();
    @FXML
    private Button b13 = new Button();
    @FXML
    private Button b14 = new Button();
    @FXML
    private Button b15 = new Button();
    @FXML
    private Button b16 = new Button();
    @FXML
    private Button b17 = new Button();
    @FXML
    private Button b18 = new Button();
    @FXML
    private Button b19 = new Button();
    @FXML
    private Button b20 = new Button();
    @FXML
    private Button b21 = new Button();
    @FXML
    private Button b22 = new Button();
    @FXML
    private Button b23 = new Button();
    @FXML
    private Button b24 = new Button();
    @FXML
    private Button b25 = new Button();
    @FXML
    private Button b26 = new Button();
    @FXML
    private Button b27 = new Button();
    @FXML
    private Button b36 = new Button();
    @FXML
    private Button b37 = new Button();
    @FXML
    private Button b38 = new Button();
    @FXML
    private Button b39 = new Button();
    @FXML
    private Button b40 = new Button();
    @FXML
    private Button b41 = new Button();
    @FXML
    private Button b42 = new Button();
    @FXML
    private Button b43 = new Button();
    @FXML
    public Button b44 = new Button();

    private ObservableList<Node> nodeList = null;

    private int position = -1;

    public FxBoardController() {
    }

    @FXML
    public void repaint() {
        nodeList = FXCollections.observableArrayList(grid.getChildren().sorted());
        this.sortNode(nodeList, 2, 12);
        this.sortNode(nodeList, 3, 23);
        this.sortNode(nodeList, 4, 34);
        this.sortNode(nodeList, 5, 40);
        this.sortNode(nodeList, 6, 41);
        this.sortNode(nodeList, 7, 42);
        this.sortNode(nodeList, 8, 43);
        this.sortNode(nodeList, 9, 44);

        System.out.println(Board.getNode(3).getPawn().getColor());
        for (int i = 0; i < 45; i++) {
            String numerator = Integer.toString(i);
            if (!Board.getNode(i).isPawnOnNode()) {
                nodeList.get(i).setVisible(false);
                continue;
            }
            if (Board.getNode(i).getPawn().getColor().equals(Color.BLACK)) {
                Button button = (Button) nodeList.get(i);
                button.setGraphic(new ImageView("sample/Images/Pionek_B.png"));
                nodeList.get(i).setVisible(true);
            }
            if (Board.getNode(i).getPawn().getColor().equals(Color.WHITE)) {
                Button button = (Button) nodeList.get(i);
                button.setGraphic(new ImageView("sample/Images/Pionek_Cz.png"));
                nodeList.get(i).setVisible(true);
            }
        }
    }

    private ObservableList<Node> sortNode(ObservableList<Node> list, int placeTo, int placeFrom) {
        list.add(placeTo, list.get(placeFrom));
        list.remove(++placeFrom);

        return list;
    }

    @FXML
    public int mouseEntered(double clickedX, double clickedY) {
        int x = (int) clickedX / 83;
        int y = (int) clickedY / 100;
        return selectPosition(x, y);
    }

    private int selectPosition(int x, int y) {
        int[][] multi = new int[][]{
                {0, 1, 2, 3, 4, 5, 6, 7, 8},
                {9, 10, 11, 12, 13, 14, 15, 16, 17},
                {18, 19, 20, 21, 22, 23, 24, 25, 26},
                {27, 28, 29, 30, 31, 32, 33, 34, 35},
                {36, 37, 38, 39, 40, 41, 42, 43, 44}
        };
        return multi[y][x];
    }

    // usuniety static
    @FXML
    private void initialize() {
        repaint();
    }

    @FXML
    private void onPawnClick(ActionEvent event) {
        Button button = (Button) event.getSource();
        position = id2Position(button.getId());
        System.out.println(position);
        Logic.processTurn(position);
        State.printGameState();
    }

    private int id2Position(String id) {
        return Integer.valueOf(id.substring(1));
    }
}
