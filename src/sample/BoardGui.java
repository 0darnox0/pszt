package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Board;
import model.Logic;
import model.State;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;

public class BoardGui extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BoardGui.fxml"));

        Parent root = (Parent) fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 750, 500));
        Scene scene = primaryStage.getScene();
        scene.getStylesheets().add("sample/cssBoard.css");
        FxBoardController fxBoardController =  fxmlLoader.getController();

        State.printGameState();
        // Reaguje na klikniecia na planszy w wszystkie miejsca oprocz przyciskow
        scene.setOnMousePressed(new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                System.out.println("Clicked X: " + event.getSceneX() + "Y: " + event.getSceneY());
                Integer liczba = fxBoardController.mouseEntered(event.getSceneX(),event.getSceneY());
                Logic.processTurn(fxBoardController.mouseEntered(event.getSceneX(),event.getSceneY()));
                Board.printColorsOnBoard();
                fxBoardController.repaint();
            }
        });

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
